const Koa = require('koa');
const convert = require('koa-convert');

const app = new Koa();

const router = require('./routes/index');

app
.use(router.routes())
.use(router.allowedMethods())
.use(async (ctx, next) => {
  console.log(ctx.body);
  // this.body = [
  //   { message: '404, page not found',status: 404 },
  // ];
  // this.status = 404;
});

// app.use(async ctx => {
//   console.log(ctx.body);
//   ctx.body = 'Hello World';
// });

app.listen(3000);