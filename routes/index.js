const Router = require('koa-router');

const router = new Router();

const data = [
  {
    name: 'ReactLearning interesting things',
    timestamp: new Date().getTime(),
    folder: 'Frontend',
    tags: ['interesting', 'tolearn', '2017', 'coolthings'],
  },
  {
    name: 'PHP so boring..',
    timestamp: new Date().getTime(),
    folder: 'Backend',
    tags: ['interesting', 'tolearn', '2017', 'coolthings'],
  },
  {
    name: 'Hohoho this is new year',
    timestamp: new Date().getTime(),
    folder: 'New Year',
    tags: ['interesting', '2017'],
  },
  {
    name: 'ReactLearning interesting things',
    timestamp: new Date().getTime(),
    folder: 'Frontend',
    tags: [],
  },
  {
    name: 'Nokia',
    timestamp: new Date().getTime(),
    folder: 'Phones',
    tags: ['GPRS', 'GSM', '1986'],
  },
  {
    name: 'Cook',
    timestamp: new Date().getTime(),
    folder: 'Potatoes',
    tags: ['interesting', 'cook2017', 'workforwomen'],
  },
  {
    name: 'ReactLearning interesting things',
    timestamp: new Date().getTime(),
    folder: 'Frontend',
    tags: ['interesting', 'tolearn', '2017', 'coolthings'],
  },
  {
    name: 'NodeJS',
    timestamp: new Date().getTime(),
    folder: 'Backend',
    tags: ['2017', 'coolthings'],
  },
  {
    name: 'Vue js THE BEST',
    timestamp: new Date().getTime(),
    folder: 'Frontend',
    tags: ['2017TOPframeworks', '2017', 'mustknow'],
  },
  {
    name: 'Angular no bad',
    timestamp: new Date().getTime(),
    folder: 'Frontend',
    tags: ['frontend'],
  },
];

router.get('/bookmarks', async (ctx, next) => {
  try {
    ctx.response.body = data;
  } catch(err) {
    console.log(err);
  }
});

module.exports = router;
